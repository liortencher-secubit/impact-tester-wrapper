﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using ImpactTester.BL.Models.Arduino;
using ImpactTester.BL.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class ConnectController : MasterController
    {
        private IImpactTesterConnectorRepository _conRepo;
        public ConnectController(IImpactTesterConnectorRepository conRepo)
        {
            _conRepo = conRepo;
        }
        public IHttpActionResult Post()
        {
            Response resp = _conRepo.ConnectImpactTester();
            return new CustomHttpResponse(Request, resp);
        }


    }
}
