﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using ImpactTester.BL.Models.Request.Tester;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class TesterController : MasterController
    {
        private IImpactTesterRepository _repo;
        public TesterController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }
        public IHttpActionResult Post(TesterRequest data)
        {
            var resp = _repo.BuildTest(data);
            return new CustomHttpResponse(Request, resp);
        }

        [Route("api/FinishTest")]
        public IHttpActionResult Get(Guid testGuid)
        {
            var resp = _repo.FinishTest(testGuid);
            return new CustomHttpResponse(Request, resp);
            //var resp = _repo.GetTest(testGuid);
            //return new CustomHttpResponse(Request, resp);
        }


    }
}
