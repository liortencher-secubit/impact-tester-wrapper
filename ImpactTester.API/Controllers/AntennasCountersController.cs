﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class AntennasCountersController : MasterController
    {
        private IImpactTesterRepository _repo;

        public AntennasCountersController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }

        public IHttpActionResult Get(string portName)
        {
            var resp = _repo.GetAntennasCounters(portName);
            return new CustomHttpResponse(Request, resp);
        }
    }
}
