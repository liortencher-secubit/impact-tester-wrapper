﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using ImpactTester.BL.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class IdnsController : MasterController
    {
        private IImpactTesterConnectorRepository _conRepo;

        public IdnsController(IImpactTesterConnectorRepository conRepo)
        {
            _conRepo = conRepo;
        }

        //public IHttpActionResult Get()
        //{
        //    _conRepo.TestFunc();
        //    return Ok();
        //}
        public IHttpActionResult Post()
        {
            var resp = _conRepo.GetIdns();
            return new CustomHttpResponse(Request, resp);
        }
    }
}
