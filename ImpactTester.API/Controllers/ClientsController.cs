﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class ClientsController : MasterController
    {
        private IImpactTesterRepository _repo;
        public ClientsController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }
        public IHttpActionResult Get()
        {
            var resp = _repo.GetClients();
            return new CustomHttpResponse(Request, resp);
        }
    }
}
