﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class PartController : MasterController
    {
        private IImpactTesterRepository _repo;
        public PartController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }
        public IHttpActionResult Get()
        {
            var resp = _repo.GetParts();
            return new CustomHttpResponse(Request, resp);            
        }
    }
}
