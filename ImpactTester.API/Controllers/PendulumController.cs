﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class PendulumController : MasterController
    {
        private IImpactTesterRepository _repo;

        public PendulumController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }
        public IHttpActionResult Get(bool isOpen)
        {
            //return Ok();
            var resp = _repo.Pendulum(isOpen);
            return new CustomHttpResponse(Request, resp);
        }
    }
}
