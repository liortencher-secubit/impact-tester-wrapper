﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class HardwareVersionController : MasterController
    {
        private IImpactTesterRepository _repo;
        public HardwareVersionController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }
        public IHttpActionResult Get()
        {
            var resp = _repo.GetHardwareVersions();
            return new CustomHttpResponse(Request, resp);
        }
    }
}
