﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using ImpactTester.BL.Models.Request.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class StepsController : MasterController
    {
        private IImpactTesterRepository _repo;

        public StepsController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }

        public IHttpActionResult Get()
        {            
            var resp = _repo.GetImpactTesterRunSteps();
            return new CustomHttpResponse(Request, resp);
        }

        public IHttpActionResult Post(StepRequest data)
        {            
            var resp = _repo.RunImpactTest(data);
            return new CustomHttpResponse(Request, resp);
        }
    }
}
