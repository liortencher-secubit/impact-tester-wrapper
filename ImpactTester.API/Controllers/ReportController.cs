﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using ImpactTester.BL.Models.Request.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImpactTester.API.Controllers
{
    public class ReportController : ApiController
    {
        private IImpactTesterRepository _repo;
        public ReportController(IImpactTesterRepository repo)
        {
            _repo = repo;
        }
        //[Route("api/GetAllTestsGuids")]
        public IHttpActionResult Get()
        {
            var resp = _repo.GetAllTestsGuides();
            return new CustomHttpResponse(Request, resp);
        }

        public IHttpActionResult Post(ReportRequest data)
        {
            var resp = _repo.GetReports(data);
            return new CustomHttpResponse(Request, resp);
        }
    }
}
