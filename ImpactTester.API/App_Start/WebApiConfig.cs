﻿using ImpactTester.API.Models;
using ImpactTester.BL;
using ImpactTester.BL.Models.Arduino;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using Unity;
using Unity.Lifetime;

namespace ImpactTester.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Unity
            var container = new UnityContainer();
            container.RegisterType<IImpactTesterRepository, ImpactTesterRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IImpactTesterConnectorRepository, ImpactTesterConnectorRepository>(new HierarchicalLifetimeManager());
            //ContainerControlledLifetimeManager
            config.DependencyResolver = new UnityResolver(container);
            // Web API configuration and services
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            // Web API routes
            EnableCors(config);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }


        public static void EnableCors(HttpConfiguration config)
        {
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }

    }
}
