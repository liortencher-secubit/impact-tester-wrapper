﻿using ImpactTester.BL.Models;
using ImpactTester.BL.Models.Arduino;
using ImpactTester.BL.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ImpactTester.API.Models
{
    public class CustomHttpResponse : IHttpActionResult
    {
        private readonly HttpRequestMessage _request;
        private readonly Response _resp;


        public CustomHttpResponse(HttpRequestMessage request, Response resp)
        {
            _request = request;
            _resp = resp;            
        }
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = _request.CreateResponse(_resp.StatusCode, _resp.Data);
            return Task.FromResult(response);
        }
    }
}