﻿using ImpactTester.BL.Models.Common;
using ImpactTester.BL.Models.Response;
using System;
using System.Collections.Generic;


namespace ImpactTester.BL
{
    public interface IImpactTesterConnectorRepository
    {
        Response ConnectImpactTester();
        Response GetIdns();

        List<SendReciveResponse> SendRecive(List<SendReciveRequest> commands, Guid testID);


        Response CloseAllCommand(string portName);
        Response OpenAntennaCommand(List<AntennaCommand> commands, bool isOpen, string portName);


        //void TestFunc();
    }
}
