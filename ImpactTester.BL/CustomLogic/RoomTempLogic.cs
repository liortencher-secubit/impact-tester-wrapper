﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpactTester.BL.Models.Common;
using ImpactTester.Connector;
using ImpactTester.BL.Utils;

namespace ImpactTester.BL.CustomLogic
{
    public class RoomTempLogic : ICustomLogic
    {
        public Dictionary<string, string> ExecCustomLogic(SendReciveRequest req, Guid testID)
        {
            int result = -1;
            var dic = new Dictionary<string, string>();
            try
            {
                //read gth block

                StringBuilder myCmdString = new StringBuilder(256).Append("0A202900");
                StringBuilder strAnswer = new StringBuilder(256);
                result = ImpactConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                if (result != 0)
                {
                    dic.Add("result", result.ToString());
                    dic.Add("strAnswer", strAnswer.ToString());
                    return dic;
                }

                string temp = strAnswer.ToString().Substring(6);
                temp = temp.Substring(0, (temp.Length - 6));
                var arrayTemp = temp.SplitInParts(2).ToArray();

                //
                //
                byte roomTemp = 0;
                using (ImpactTesterEntities db = new ImpactTesterEntities())
                {
                    roomTemp = db.dt_ImpactTest.FirstOrDefault(x => x.TestID == testID).RoomTemp;
                }
                arrayTemp[2] = roomTemp.ToString("XX");
                //
                myCmdString = new StringBuilder(256).Append($"0A212900{string.Join("", arrayTemp)}");
                strAnswer = new StringBuilder(256);
                result = ImpactConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                dic.Add("strAnswer", strAnswer.ToString());
            }
            catch (Exception)
            {
                result = -1;
            }
            dic.Add("result", result.ToString());
            return dic;
        }
    }
}
