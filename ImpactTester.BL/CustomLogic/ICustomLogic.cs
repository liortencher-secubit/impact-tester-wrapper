﻿using ImpactTester.BL.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.CustomLogic
{
    interface ICustomLogic
    {
        Dictionary<string, string> ExecCustomLogic(SendReciveRequest req, Guid testID);
    }
}
