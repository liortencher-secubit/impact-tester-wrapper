﻿
using ImpactTester.BL.Models.Request.Report;
using ImpactTester.BL.Models.Request.Steps;
using ImpactTester.BL.Models.Request.Tester;
using ImpactTester.BL.Models.Response;
using System;

namespace ImpactTester.BL
{
    public interface IImpactTesterRepository
    {
        Response GetAntennasCounters(string portName);
        Response BuildTest(TesterRequest data);
        //Response GetTest(Guid testGuid);
        Response GetImpactTesterRunSteps();
        Response RunImpactTest(StepRequest data);
        Response FinishTest(Guid testGuid);
        //Common
        Response GetHardwareVersions();
        Response GetParts();
        Response GetClients();
        Response GetPorts();
        Response Pendulum(bool isOpen);
        //Report
        Response GetAllTestsGuides();
        Response GetReports(ReportRequest data);
    }
}
