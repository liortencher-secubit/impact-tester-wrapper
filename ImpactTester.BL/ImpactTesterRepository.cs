﻿using ImpactTester.BL.Models.Common;
using ImpactTester.BL.Models.Request.Report;
using ImpactTester.BL.Models.Request.Steps;
using ImpactTester.BL.Models.Request.Tester;
using ImpactTester.BL.Models.Response;
using ImpactTester.BL.Models.Response.AntennasCounters;
using ImpactTester.BL.Models.Response.Steps;
using ImpactTester.BL.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Threading;

namespace ImpactTester.BL
{
    public class ImpactTesterRepository : IImpactTesterRepository
    {
        private ImpactTesterEntities db;
        private ImpactTesterConnectorRepository conRepo;
        public ImpactTesterRepository()
        {
            db = new ImpactTesterEntities();
            conRepo = new ImpactTesterConnectorRepository();
        }

        public Response GetAntennasCounters(string portName)
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                var antennas = db.dt_Antenna.OrderBy(x => x.AntennaName).Select(x => new AntennasCountersResponse
                {
                    Antenna = new AntennaModel
                    {
                        AntennaID = x.AntennaID,
                        AntennaName = x.AntennaName,
                        AntennaCommandsString = x.AntennaCommand,
                        //AntennaCommands = new List<AntennaCommand>()
                    },
                    SendReciveResponse = new SendReciveResponse()
                }).ToList();
                conRepo.CloseAllCommand(portName);
                foreach (var antenna in antennas)
                {
                    antenna.Antenna.AntennaCommands = JsonConvert.DeserializeObject<List<AntennaCommand>>(antenna.Antenna.AntennaCommandsString);

                    conRepo.OpenAntennaCommand(antenna.Antenna.AntennaCommands, true, portName);
                    Thread.Sleep(10);
                    var sendRec = conRepo.SendRecive(new List<SendReciveRequest>
                    {
                        new SendReciveRequest {
                            CommandName = "inventory",
                            Command = "260100"
                        }
                    }, Guid.Empty).FirstOrDefault();
                    if (sendRec.IsSuccess)
                    {
                        antenna.Counter = new CounterModel
                        {
                            CounterSerial = sendRec.Msg
                        };
                    }
                    antenna.SendReciveResponse = sendRec;
                    conRepo.OpenAntennaCommand(antenna.Antenna.AntennaCommands, false, portName);
                }
                //conRepo.CloseAllCommand(portName);
                resp.Data = antennas;
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        public Response BuildTest(TesterRequest data)
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                Guid testGuid = Guid.NewGuid();
                db.dt_ImpactTest.Add(new dt_ImpactTest
                {
                    TestID = testGuid,
                    StartDate = DateTime.Now,
                    OperatorName = data.OperatorName,
                    Part_PartID = data.PartNumber,
                    HardwareVersion_ID = data.hardwareVersion,
                    Client_ClientID = data.ClientID,
                    BatchNumber = data.BatchNumber,
                    RoomTemp = data.RoomTemp
                });
                foreach (var counter in data.Counters)
                {
                    if (!string.IsNullOrEmpty(counter.CounterSerial))
                    {
                        db.dt_Counters.Add(new dt_Counters
                        {
                            ImpactTest_TestID = testGuid,
                            CounterSerial = counter.CounterSerial,
                            Antenna_AntennaID = counter.AntennaID
                        });
                    }
                }
                db.SaveChanges();
                resp.Data = new
                {
                    TestGuid = testGuid
                };
            }
            catch (Exception ex)
            {

                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        //public Response GetTest(Guid testGuid)
        //{
        //    Response resp = new Response();
        //    try
        //    {
        //        resp.StatusCode = HttpStatusCode.OK;
        //        var data = db.dt_Counters.Where(x => x.dt_ImpactTest.TestID == testGuid).Select(y => new TesterResponse
        //        {
        //            AntennaID = y.Antenna_AntennaID,
        //            AntennaName = y.dt_Antenna.AntennaName,
        //            CounterSerial = y.CounterSerial
        //        }).ToList();
        //        resp.Data = data;
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.StatusCode = HttpStatusCode.InternalServerError;
        //    }
        //    return resp;
        //}

        public Response GetImpactTesterRunSteps()
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;

                var data = db.dt_Step.Select(x => new StepResponse
                {
                    StepName = x.Name,
                    StepID = x.StepID,
                    StepCommands = x.dt_StepCommands.Where(d => d.Disabled == false).OrderBy(z => z.ExecOrder).Select(y => new StepCommandResponse
                    {
                        CommandID = y.CommandID,
                        CommandName = y.Name,
                        CmdExec = y.CmdExec,
                        CommandOperator = y.CommandOperator,
                        CommandExceptResult = y.CommandExceptResult,
                        ExecOrder = y.ExecOrder,
                        IsLastStep = y.IsLastStep,
                        IsCustomLogic = y.IsCustomLogic,
                        CustomLogicCmd = y.CustomLogicCmd
                    }).ToList()
                }).ToList();
                resp.Data = data;
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        public Response RunImpactTest(StepRequest data)
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                //remove steps
                if (data.StepID == 1)
                {
                    var ConnectedCountersSerials = data.ConnectedCounters.Select(x => x.Counter.CounterSerial).ToList();
                    var dbCounters = db.dt_Counters.Where(x => ConnectedCountersSerials.Any(y => y.ToLower() == x.CounterSerial.ToLower()) && x.dt_ImpactTest.TestID == data.TestID).Select(z => z.CounterID).ToList();
                    db.dt_CounterSteps.RemoveRange(db.dt_CounterSteps.Where(x => dbCounters.Any(y => y == x.Counter_CounterID)));
                    //foreach (var counter in data.ConnectedCounters)
                    //{
                    //    FirstOrDefault(x => x.CounterSerial.ToLower() == counter.Counter.CounterSerial.ToLower() &&
                    //            x.dt_ImpactTest.TestID == data.TestID);
                    //}
                }
                //
                ExecStepResponse execData = new ExecStepResponse();
                execData.StepID = data.StepID;
                execData.ConnectedCounters = new List<ExecConnectedCounter>();

                //build step commands
                List<SendReciveRequest> commands = new List<SendReciveRequest>();
                foreach (var command in data.StepCommands)
                {
                    commands.Add(new SendReciveRequest
                    {
                        CommandID = command.CommandID,
                        Command = command.CmdExec,
                        CommandName = command.CommandName.Replace(" ", ""),
                        CommandOperator = command.CommandOperator,
                        CommandExceptResult = command.CommandExceptResult,
                        CommandExecOrder = command.ExecOrder,
                        IsLastStep = command.IsLastStep,
                        IsCustomLogic = command.IsCustomLogic,
                        CustomLogicCmd = command.CustomLogicCmd
                    });
                    if (command.IsLastStep)
                    {
                        var client = db.dt_ImpactTest.FirstOrDefault(x => x.TestID == data.TestID).dt_Clients;
                        commands.Add(new SendReciveRequest
                        {
                            //CommandID = command.CommandID,
                            Command = client.ClientEncryptionCmd,
                            CommandName = "setpassword",
                            CommandOperator = "==",
                            CommandExceptResult = client.ClientEncryptionCmdResult,
                            //CommandExecOrder = command.ExecOrder,
                            //IsLastStep = command.IsLastStep
                        });
                    }
                }
                //bool isLastStep = (commands.FirstOrDefault(x => x.IsLastStep == true) != null);
                //dt_Clients client = null;
                //if (isLastStep)
                //{

                //}
                foreach (var counter in data.ConnectedCounters)
                {
                    //                    
                    var dbCounter = db.dt_Counters.FirstOrDefault(x => x.CounterSerial.ToLower() == counter.Counter.CounterSerial.ToLower() &&
                    x.dt_ImpactTest.TestID == data.TestID);
                    //todo tell which counter to connect
                    //conRepo.CloseAllCommand(data.PortName);
                    conRepo.OpenAntennaCommand(counter.Antenna.AntennaCommands, true, data.PortName);
                    Thread.Sleep(10);


                    List<SendReciveResponse> results = conRepo.SendRecive(commands, data.TestID);
                    //var isFailed = results.FirstOrDefault(x => x.IsSuccess == false);
                    //                           
                    foreach (var result in results)
                    {
                        var newStep = new dt_CounterSteps
                        {
                            Counter_CounterID = dbCounter.CounterID,
                            Step_StepID = data.StepID,
                            Step_ExecID = result.CommandExecOrder
                        };
                        //bool isPass = false;
                        //
                        newStep.RawResult = (!string.IsNullOrEmpty(result.RawValue) ? result.RawValue : null);
                        //
                        if (!result.IsSuccess)
                        {
                            newStep.FailedDescription = result.Msg;
                        }
                        else
                        {
                            newStep.PassDescription = result.Msg; //string.Join(", ", results.Select(x => x.Msg));                          
                            newStep.IsPass = true;
                        }
                        //newStep.IsPass = isPass;
                        //
                        db.dt_CounterSteps.Add(newStep);
                        //
                        execData.ConnectedCounters.Add(new ExecConnectedCounter
                        {
                            CounterSerial = dbCounter.CounterSerial,
                            IsPass = result.IsSuccess,
                            resultMsg = string.Join("\n", results.Select(x => x.Msg))
                        });
                        //
                        resp.Data = execData;
                    }
                    //if (isLastStep)
                    //{

                    //}
                    //
                    db.SaveChanges();
                    conRepo.OpenAntennaCommand(counter.Antenna.AntennaCommands, false, data.PortName);
                }
                //conRepo.CloseAllCommand(data.PortName);

            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        public Response FinishTest(Guid testGuid)
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                var test = db.dt_ImpactTest.FirstOrDefault(x => x.TestID == testGuid);
                test.EndDate = DateTime.Now;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        //Reports

        public Response GetAllTestsGuides()
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                var data = db.dt_ImpactTest.OrderByDescending(x => x.StartDate).Select(x => x.TestID).ToList();
                resp.Data = data;
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;

        }

        public Response GetReports(ReportRequest request)
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;

                IQueryable<dt_ImpactTest> query = db.dt_ImpactTest.Include("dt_Counters.dt_CounterSteps.dt_Step");
                if (request.TestGuid != Guid.Empty)
                {
                    query = query.Where(x => x.TestID == request.TestGuid);
                }
                else
                {
                    query.Where(x =>
                        DbFunctions.TruncateTime(x.StartDate) >= DbFunctions.TruncateTime(request.StartDate) &&
                        DbFunctions.TruncateTime(x.EndDate) <= DbFunctions.TruncateTime(request.EndDate));
                }
                //
                var data = query.Select(x => new
                {
                    TestID = x.TestID,
                    EndDate = x.EndDate,
                    TesterName = x.OperatorName,
                    MechanicalAssemby = x.dt_Part.PartName,
                    SWVersionChecked = x.dt_HardwareVersion.HarwareVersionName,
                    RoomTemp = x.RoomTemp,
                    EnCode = x.dt_Clients.EncryptionCode,
                    Counters = x.dt_Counters.OrderBy(ord => ord.CounterID).Select(cnt => new
                    {
                        CounterSerial = cnt.CounterSerial,
                        Status = cnt.dt_CounterSteps.Select(stSelect => new
                        {
                            IsPass = stSelect.IsPass,
                            FailedStepName = stSelect.dt_Step.Name,
                            FailedDescription = stSelect.FailedDescription,
                            PassedDescription = stSelect.PassDescription,
                            StepID = stSelect.Step_StepID,
                            ExecId = stSelect.Step_ExecID,
                            RawResult = stSelect.RawResult
                        }).ToList()
                    }).ToList()
                }).ToList();


                List<object> reportData = new List<object>();
                foreach (var report in data)
                {
                    foreach (var counter in report.Counters)
                    {
                        var batteryStart = counter.Status.FirstOrDefault(x => x.StepID == 1 && x.ExecId == 1);
                        var batteryEnd = counter.Status.FirstOrDefault(x => x.StepID == 4 && x.ExecId == 7);
                        var gth = counter.Status.FirstOrDefault(x => x.StepID == 4 && x.ExecId == 10);
                        bool isPass = counter.Status.FirstOrDefault(x => x.IsPass == false) == null;
                        reportData.Add(new
                        {
                            RFID = counter.CounterSerial,
                            TestDate = (report.EndDate != null ? report.EndDate.Value.ToString() : string.Empty),
                            TesterName = report.TesterName,
                            Mechanical = report.MechanicalAssemby,
                            SWVersion = report.SWVersionChecked,
                            RFIDTest = "Pass",
                            BatteryVoltage = ((batteryStart != null && !batteryStart.IsPass) ? batteryStart.RawResult : (batteryEnd != null ? batteryEnd.RawResult : batteryStart.RawResult)),
                            PassTest = (isPass ? "Pass" : "Failed"),
                            Temprature = report.RoomTemp,
                            EnCode = report.EnCode,
                            GTH = (gth == null ? string.Empty : "Pass")
                        });
                    }
                }


                resp.Data = reportData;

            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        private dynamic BuildException(Exception ex)
        {
            return new
            {
                Ex = ex?.Message?.ToString(),
                InnerException = ex?.InnerException?.ToString(),
                StackTrace = ex?.StackTrace?.ToString()
            };
        }

        public Response GetHardwareVersions()
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                resp.Data = db.dt_HardwareVersion.Select(x => new
                {
                    ID = x.HarwareVersionID,
                    Value = x.HarwareVersionName
                }).ToList();
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }

        public Response GetParts()
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                resp.Data = db.dt_Part.Select(x => new
                {
                    ID = x.PartID,
                    Value = x.PartName
                }).ToList();

            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }
        public Response GetClients()
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                resp.Data = db.dt_Clients.Select(x => new
                {
                    ID = x.ClientID,
                    Value = x.ClientName
                }).ToList();
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }


        public Response GetPorts()
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;

                using (SerialPort port = new SerialPort())
                {
                    string[] ports = SerialPort.GetPortNames();
                    resp.Data = ports;
                }
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;



        }

        public Response Pendulum(bool isOpen)
        {
            Response resp = new Response();
            try
            {
                resp.StatusCode = HttpStatusCode.OK;
                string dataCmd = isOpen ? ConfigurationManager.AppSettings["PendulumOpen"].ToString() :
                    ConfigurationManager.AppSettings["PendulumClose"].ToString();

                conRepo.OpenAntennaCommand(new List<AntennaCommand> { new AntennaCommand { Command = (isOpen ? "open" : "close"), Data = dataCmd } }, isOpen, string.Empty);
                if (!isOpen)
                {
                    Thread.Sleep(1000);
                }
                resp.Data = isOpen;
            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
                resp.Data = BuildException(ex);
            }
            return resp;
        }
    }
}
