﻿using ImpactTester.BL.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Validators
{
    public interface IValidator
    {
        void Validate(SendReciveResponse res, SendReciveRequest req);
    }
}
