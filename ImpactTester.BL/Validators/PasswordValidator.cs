﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpactTester.BL.Models.Common;

namespace ImpactTester.BL.Validators
{
    public class PasswordValidator : IValidator
    {
        public void Validate(SendReciveResponse res, SendReciveRequest req)
        {
            res.IsSuccess = req.CommandExceptResult.Equals(res.Msg?.Value);
            if (!res.IsSuccess)
            {
                if (req.CommandName == "setpassword")
                {
                    res.Msg = "Set Password Error.";
                }
                else
                {
                    res.Msg = "Present Password Error.";
                }
            }
            else
            {
                res.RawValue = res.Msg?.Value;
                res.Msg = res.Msg?.Label.ToString();

            }
        }
    }
}
