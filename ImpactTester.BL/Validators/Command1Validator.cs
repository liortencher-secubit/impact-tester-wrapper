﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpactTester.BL.Models.Common;
using ImpactTester.BL.Utils;

namespace ImpactTester.BL.Validators
{
    public class Command1Validator : IValidator
    {
        public void Validate(SendReciveResponse res, SendReciveRequest req)
        {
            res.IsSuccess = ImpactTesterUtils.ParseHexToNumber(res.Msg?.Value) >= decimal.Parse(req.CommandExceptResult);
            string value = ((float)(ImpactTesterUtils.ParseHexToNumber(res.Msg?.Value.ToString()) / 100)).ToString("0.00");
            res.RawValue = value;
            if (!res.IsSuccess)
            {
                res.Msg = "Battery level is not current";                
            }
            else
            {
                res.Msg = $"{res.Msg?.Label.ToString()} {value}";
            }
        }
    }
}
