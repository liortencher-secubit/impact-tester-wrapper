﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpactTester.BL.Models.Common;
using ImpactTester.BL.Utils;

namespace ImpactTester.BL.Validators
{
    public class Command4Validator : IValidator
    {
        public void Validate(SendReciveResponse res, SendReciveRequest req)
        {
            switch (req.CommandOperator)
            {
                case "==":
                    res.IsSuccess = res.Msg?.Value == decimal.Parse(req.CommandExceptResult);
                    res.Msg = !res.IsSuccess ? "Single shot after reset not equals to zero." : res.Msg;
                    break;
                case ">":
                    res.IsSuccess = res.Msg?.Value > decimal.Parse(req.CommandExceptResult);
                    res.Msg = !res.IsSuccess ? "Single shot after pendulum not bigger then zero." : res.Msg;
                    break;
                default:
                    break;
            }
            if (res.IsSuccess)
            {
                res.RawValue = res.Msg?.Value.ToString();
                res.Msg = res.Msg?.Label.ToString() + " " + res.Msg?.Value.ToString();
                
            }
            //res.IsSuccess = res.Msg?.Value == decimal.Parse(req.CommandExceptResult);
            //if (!res.IsSuccess)
            //{
            //    res.Msg = "Single shot after reset not equals to zero.";
            //}
            //else
            //{
            //    res.Msg = res.Msg?.Label.ToString() + " " + res.Msg?.Value.ToString();
            //}
        }
    }
}
