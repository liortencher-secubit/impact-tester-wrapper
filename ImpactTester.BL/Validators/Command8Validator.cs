﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImpactTester.BL.Models.Common;

namespace ImpactTester.BL.Validators
{
    public class Command8Validator : IValidator
    {
        public void Validate(SendReciveResponse res, SendReciveRequest req)
        {
            res.IsSuccess = res.Msg?.Value == decimal.Parse(req.CommandExceptResult);
            if (!res.IsSuccess)
            {
                res.Msg = "Single shot after reset not equals to zero.";
            }
            else
            {
                res.Msg = res.Msg?.Label.ToString() + " " + res.Msg?.Value.ToString();
            }
        }
    }
}
