﻿
using ImpactTester.BL.CustomLogic;
using ImpactTester.BL.Models.Common;
using ImpactTester.BL.Models.Response;
using ImpactTester.BL.Utils;
using ImpactTester.BL.Validators;
using ImpactTester.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;


namespace ImpactTester.BL
{
    public class ImpactTesterConnectorRepository : IImpactTesterConnectorRepository
    {
        private IValidator Validator;
        //private Arduino _arduino;
        public ImpactTesterConnectorRepository()
        {
            //_arduino = new Arduino();
        }
        public Response ConnectImpactTester()
        {

            //AdvantechConnector con = new AdvantechConnector();


            bool isPassConnected = true;
            StringBuilder msg = new StringBuilder();
            //
            long result = ImpactConnector.CR95HFDLL_USBconnect();
            if (result == 0)
            {
                msg.Append("CR95HF demonstration board connected and ready to be used.\n");
            }
            else
            {
                isPassConnected = false;
                msg.Append("CR95HF demonstration board not connected.\n");
            }
            //
            result = ImpactConnector.CR95HFDLL_USBhandlecheck();
            if (result == 0)
            {
                msg.Append("The USB handle is valid.\n");
            }
            else
            {
                isPassConnected = false;
                msg.Append("The USB handle is not valid.\n");
            }
            //
            StringBuilder strAnswer = new StringBuilder(256);
            result = ImpactConnector.CR95HFDll_GetDLLrev(strAnswer);
            if (result == 0)
            {
                msg.Append($"Revision of the DLL installed on your PC system - {strAnswer.ToString()}.\n");
            }
            else
            {
                msg.Append($"Revision of the DLL not found.\n");
            }
            //
            StringBuilder selectAnswer = new StringBuilder(256);
            StringBuilder myCmdString = new StringBuilder(256);
            myCmdString.Append("010D");
            ImpactConnector.CR95HFDll_Select(myCmdString, selectAnswer);

            Response resp = new Response();
            //
            //Response arduConnectResp = ardu.Connect();
            //if (arduConnectResp.StatusCode == HttpStatusCode.OK)
            //{
            //    msg.Append($"Arduino is connected successfully.\n");
            //}
            //else
            //{
            //    isPassConnected = false;
            //    //resp.Data = arduConnectResp.Data;
            //}
            //Response arduDisconnectResp = ardu.Disconnect();
            //if (arduConnectResp.StatusCode == HttpStatusCode.OK)
            //{
            //    msg.Append($"Arduino disconnected successfully.\n");
            //}
            //else
            //{
            //    isPassConnected = false;
            //    //resp.Data = arduConnectResp.Data;
            //}

            if (isPassConnected)
            {
                resp.StatusCode = HttpStatusCode.OK;

            }
            else
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }
            resp.Data = new { Msg = msg.ToString() };
            return resp;
        }


        public Response GetIdns()
        {
            StringBuilder strAnswer = new StringBuilder(256);
            var result = ImpactConnector.CR95HFDll_Idn(strAnswer);
            switch (result)
            {
                case 0:
                    return new Response
                    {
                        StatusCode = HttpStatusCode.OK,
                        Data = new
                        {
                            Msg = strAnswer + "\n"
                        }
                    };
                case 5:
                    return new Response
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Data = new
                        {
                            Msg = "CR95HF demonstration board not connected.\n"
                        }
                    };
                default:
                    return new Response
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Data = new
                        {
                            Msg = "Unknown error occured.\n"
                        }
                    };
            }
        }

        public List<SendReciveResponse> SendRecive(List<SendReciveRequest> commands, Guid testID)
        {
            //try
            //{
            //ardu.Connect();
            //Thread.Sleep(500);
            List<SendReciveResponse> sendRecive = new List<SendReciveResponse>();
            bool isFail = false;
            //loop throw command
            foreach (var command in commands)
            {
                //
                int result = -1;
                StringBuilder myCmdString = null;
                StringBuilder strAnswer = null;
                //                
                if (command.IsCustomLogic)
                {
                    ICustomLogic logic = null;
                    Dictionary<string, string> resu;
                    string rs = string.Empty;
                    switch (command.CustomLogicCmd.ToLower())
                    {
                        case "gthlogic":
                            logic = new GTHLogic();
                            resu = logic.ExecCustomLogic(command, testID);
                            result = int.Parse(resu["result"]);
                            strAnswer = new StringBuilder(resu["strAnswer"]);
                            break;
                        case "roomtemp":
                            logic = new RoomTempLogic();
                            resu = logic.ExecCustomLogic(command, testID);
                            result = int.Parse(resu["result"]);
                            strAnswer = new StringBuilder(resu["strAnswer"]);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    //build command
                    myCmdString = new StringBuilder(256).Append(command.Command);
                    strAnswer = new StringBuilder(256);
                    result = ImpactConnector.CR95HFDll_SendReceive(myCmdString, strAnswer);
                    //  
                }
                switch (result)
                {
                    case 0:
                        var srResponse = new SendReciveResponse
                        {
                            Command = command?.Command,
                            Msg = ParseReturnedMessage(command.CommandName, strAnswer?.ToString()),
                            IsSuccess = true,
                            CommandExecOrder = command.CommandExecOrder
                        };
                        if (!string.IsNullOrEmpty(command.CommandOperator))
                        {
                            switch (command.CommandName.ToLower().Trim())
                            {
                                case "batterylevel":
                                    Validator = new Command1Validator();
                                    Validator.Validate(srResponse, command);
                                    if (!srResponse.IsSuccess)
                                    {
                                        srResponse.RawValue = srResponse.RawValue;
                                    }
                                    break;
                                case "singleshot":
                                    Validator = new Command4Validator();
                                    Validator.Validate(srResponse, command);
                                    break;
                                case "presentpassword":
                                    Validator = new PasswordValidator();
                                    Validator.Validate(srResponse, command);
                                    break;
                                case "setpassword":
                                    Validator = new PasswordValidator();
                                    Validator.Validate(srResponse, command);
                                    break;
                                default:
                                    break;
                            }
                            if (!srResponse.IsSuccess)
                            {
                                isFail = true;
                            }
                        }
                        else
                        {
                            srResponse.RawValue = (!string.IsNullOrEmpty(srResponse.Msg?.Value.ToString()) ? srResponse.Msg?.Value.ToString() : string.Empty);
                            srResponse.Msg = srResponse.Msg?.Label.ToString() + " " + srResponse.Msg?.Value.ToString();
                        }
                        sendRecive.Add(srResponse);
                        break;
                    case 4:
                        sendRecive.Add(new SendReciveResponse
                        {
                            Command = command?.Command,
                            IsSuccess = false,
                            Msg = $"Communication error. - {GetErrorMsgByID(strAnswer.ToString())}"
                        });
                        isFail = true;
                        break;
                    case 5:
                        sendRecive.Add(new SendReciveResponse
                        {
                            Command = command?.Command,
                            IsSuccess = false,
                            Msg = "CR95HF demonstration board not connected."
                        });
                        isFail = true;
                        break;
                    default:
                        sendRecive.Add(new SendReciveResponse
                        {
                            Command = command?.Command,
                            IsSuccess = false,
                            Msg = "Unknown error occured."
                        });
                        isFail = true;
                        break;
                }
                if (isFail)
                {
                    return sendRecive;
                }
            }
            return sendRecive;
        }

        private static string GetErrorMsgByID(string errorMsg)
        {
            string error = errorMsg.Split(':')[0].Trim();
            switch (error)
            {
                case "8200":
                    return "Invalid command length.\n";
                case "8300":
                    return "Invalid protocol.\n";
                case "8600":
                    return "Communication error.\n";
                case "8700":
                    return "Frame wait time out OR no tag.\n";
                case "8800":
                    return "Invalid Start Of Frame.\n";
                case "8900":
                    return "Receive buffer overflow (too many bytes received).\n";
                case "8A00":
                    return "Framing error (start bit = 0, stop bit = 1).\n";
                case "8B00":
                    return "EGT time out (for ISOIEC 14443-B).\n";
                case "8C00":
                    return "Invalid length. Used in Felica, when field length < 3.\n";
                case "8D00":
                    return "CRC error (Used in Felica protocol).\n";
                case "8E00":
                    return "Reception lost without EOF received.\n";
                case "8F00":
                    return "No field";
                case "FD00":
                    return "Time out - no answer from Tag detected by the CR95HF IC";
                case "FE00":
                    return "Unknown error";
                default:
                    return string.Empty;
            }
        }

        private static dynamic ParseReturnedMessage(string commandName, string message)
        {
            string cmd = string.Empty;
            switch (commandName.ToLower())
            {

                case "inventory":
                    cmd = message.Substring(8);
                    return new
                    {
                        Label = "",
                        Value = cmd.Substring(0, (cmd.Length - 10))
                    };
                //return $"{cmd.Substring(0, (cmd.Length - 10))}";
                case "systeminfo":
                    cmd = message.Substring(4);
                    return new
                    {
                        Label = "",
                        Value = cmd.Substring(0, (cmd.Length - 6))
                    };
                //return cmd.Substring(0, (cmd.Length - 6));
                case "batterylevel":
                    cmd = message.Substring(6);
                    return new
                    {
                        Label = "Battery Level:",
                        Value = cmd.Substring(0, (cmd.Length - 10))
                    };
                case "singleshot":
                    cmd = message.Substring(6);
                    return new
                    {
                        Label = "Single Shot:",
                        Value = ImpactTesterUtils.ParseHexToNumber(cmd.Substring(0, (cmd.Length - 6)))
                        //Value =  cmd.Substring(0, (cmd.Length - 6))
                    };
                //return $"Single Shot: {cmd.Substring(0, (cmd.Length - 6))}";
                case "clearallshotcounters":
                    cmd = message.Substring(6);
                    return new
                    {
                        Label = "Clear All Shot Counters Successfully.",
                        Value = cmd.Substring(0, (cmd.Length - 6))
                    };
                case "resetsingleshot":
                case "resetcurrentdry":
                case "resetcurrentmultiburst":
                case "resetcurrentburstblanks":
                case "resetcurrentsingleblanks":
                    cmd = message.Substring(6);
                    return new
                    {
                        Label = $"{commandName} Successfully.",
                        Value = cmd.Substring(0, (cmd.Length - 6))
                    };
                case "presentpassword":
                    //cmd = message.Substring(6);
                    return new
                    {
                        Label = "Present Password Successfully.",
                        Value = message
                    };
                case "resetcommand":
                    return new
                    {
                        Label = "reset command",
                        Value = message
                    };
                case "resetcommandinvalidate":
                    return new
                    {
                        Label = "reset command invalidate",
                        Value = message
                    };
                case "setpassword":
                    return new
                    {
                        Label = "Set Password Successfully.",
                        Value = message
                    };
                case "gth3defenition":
                    return new
                    {
                        Label = "GTH 3 Set Successfully.",
                        Value = string.Empty
                    };
                case "microcontrollertemprature":
                    return new
                    {
                        Label = "Temp Set Successfully.",
                        Value = string.Empty
                    };
                default:
                    return string.Empty;
            }

        }


        public Response CloseAllCommand(string portName)
        {
            Response resp = new Response();
            try
            {
                if (!string.IsNullOrEmpty(portName))
                {
                    DenkoviConnector.portName = portName;
                }
                var dc = DenkoviConnector.Instance;
                dc.setResetRelays("off//");
                resp.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resp;
        }

        public Response OpenAntennaCommand(List<AntennaCommand> commands, bool isOpen, string portName)
        {
            Response resp = new Response();
            try
            {
                if (!string.IsNullOrEmpty(portName))
                {
                    DenkoviConnector.portName = portName;
                }
                var dc = DenkoviConnector.Instance;

                string where = isOpen ? "open" : "close";
                var command = commands.FirstOrDefault(x => x.Command.ToLower() == where);
                dc.setResetRelays(command.Data);
                //foreach (var command in commands)
                //{
                //    adc.Write(command.Port, command.Data);
                //}

                resp.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                resp.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resp;
        }
    }
}
