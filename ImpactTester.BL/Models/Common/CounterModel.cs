﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Common
{
    public class CounterModel
    {
        public int CounterID { get; set; }
        public string CounterSerial { get; set; }
    }
}
