﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Common
{
    public class AntennaModel
    {
        public Guid AntennaID { get; set; }
        public string AntennaName { get; set; }
        public string AntennaCommandsString { get; set; }
        public List<AntennaCommand> AntennaCommands { get; set; }
    }
}
