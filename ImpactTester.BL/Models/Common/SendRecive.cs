﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Common
{
    public class SendRecive
    {
        public string Command { get; set; }
    }

    public class SendReciveRequest : SendRecive
    {
        public int CommandID { get; set; }
        public string CommandName { get; set; }
        public string CommandOperator { get; set; }
        public string CommandExceptResult { get; set; }
        public int CommandExecOrder { get; set; }
        public bool IsLastStep { get; set; }
        public bool IsCustomLogic { get; set; }
        public string CustomLogicCmd { get; set; }

    }

    public class SendReciveResponse : SendRecive
    {

        public bool IsSuccess { get; set; }
        public dynamic Msg { get; set; }
        public string RawValue { get; set; }
        public int CommandExecOrder { get; set; }
    }
}
