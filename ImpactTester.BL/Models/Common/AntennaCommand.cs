﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Common
{
    public class AntennaCommand
    {
        public string Command { get; set; }
        public string Data { get; set; }
    }
}
