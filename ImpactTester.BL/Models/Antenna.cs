﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models
{
    public class Antenna
    {
        public string Name { get; set; }
        public string CounterSerial { get; set; }
        public ImpactTester TestFlow { get; set; }
    }
}
