﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models
{
    public class ImpactTester
    {
        public List<Step> Steps { get; set; }
    }

    public class Step
    {
        public string StepName { get; set; }
        public string StepDescription { get; set; }
        public List<Command> Command { get; set; }
        public bool IsPass { get; set; }
    }

    public class Command
    {
        public string CommandName { get; set; }
        public string CommandExec { get; set; }
        public bool IsPass { get; set; }
        public StringBuilder Message { get; set; }
    }

}
