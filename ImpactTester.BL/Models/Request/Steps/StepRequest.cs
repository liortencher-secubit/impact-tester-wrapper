﻿using ImpactTester.BL.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Request.Steps
{
    public class StepRequest
    {
        public Guid TestID { get; set; }
        public int StepID { get; set; }
        public List<StepCommandRequest> StepCommands { get; set; }
        public List<ConnectedCounter> ConnectedCounters { get; set; }
        public string PortName { get; set; }

    }

    public class StepCommandRequest
    {
        public int CommandID { get; set; }
        public string CommandName { get; set; }
        public string CmdExec { get; set; }
        public string CommandOperator { get; set; }
        public string CommandExceptResult { get; set; }
        public int ExecOrder { get; set; }
        public bool IsLastStep { get; set; }
        public bool IsCustomLogic { get; set; }
        public string CustomLogicCmd { get; set; }
    }

    public class ConnectedCounter
    {
        public AntennaModel Antenna { get; set; }
        public CounterModel Counter { get; set; }
        //public Guid AntennaID { get; set; }
        //public string CounterSerial { get; set; }
        //public int CounterID { get; set; }
    }
}
