﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Request.Tester
{
    public class TesterRequest
    {
        public string OperatorName { get; set; }
        public byte PartNumber { get; set; }
        public byte hardwareVersion { get; set; }
        public string BatchNumber { get; set; }
        public byte ClientID { get; set; }
        public byte RoomTemp { get; set; }
        public List<ConnectedCounter> Counters { get; set; }
    }

    public class ConnectedCounter
    {
        public Guid AntennaID { get; set; }
        public string CounterSerial { get; set; }
    }
}
