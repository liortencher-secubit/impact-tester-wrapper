﻿using ImpactTester.BL.Converters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Request.Report
{
    public class ReportRequest
    {
        public Guid TestGuid { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime StartDate { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime EndDate { get; set; }
    }
}
