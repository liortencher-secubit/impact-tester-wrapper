﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Response.Steps
{
    public class ExecStepResponse
    {
        public int StepID { get; set; }
        public List<ExecConnectedCounter> ConnectedCounters { get; set; }
    }

    public class ExecConnectedCounter
    {
        public string CounterSerial { get; set; }
        public bool IsPass { get; set; }
        public string resultMsg { get; set; }
    }    
}
