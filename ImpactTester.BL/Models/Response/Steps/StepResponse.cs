﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Response.Steps
{
    public class StepResponse
    {
        public int StepID { get; set; }
        public string StepName { get; set; }
        public List<StepCommandResponse> StepCommands { get; set; }

    }

    public class StepCommandResponse
    {
        public int CommandID { get; set; }
        public string CommandName { get; set; }
        public string CmdExec { get; set; }
        public string CommandOperator { get; set; }
        public string CommandExceptResult { get; set; }
        public int? ExecOrder { get; set; }
        public bool IsLastStep { get; set; }
        public bool IsCustomLogic { get; set; }
        public string CustomLogicCmd { get; set; }
    }
}
