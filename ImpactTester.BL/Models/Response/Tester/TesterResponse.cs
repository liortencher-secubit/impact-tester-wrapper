﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Response.Tester
{
    public class TesterResponse
    {
        public Guid AntennaID { get; set; }
        public string AntennaName { get; set; }
        public string CounterSerial { get; set; }
    }
}
