﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Response
{
    public class Response
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMsg { get; set; }
        public object Data { get; set; }
    }
}
