﻿using ImpactTester.BL.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.BL.Models.Response.AntennasCounters
{
    public class AntennasCountersResponse
    {
        public AntennaModel Antenna { get; set; }
        //public Guid AntennaID { get; set; }
        //public string AntennaName { get; set; }
        public CounterModel Counter { get; set; }
        public SendReciveResponse SendReciveResponse { get; set; }
    }
}
