﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.Connector
{
    public class DenkoviConnector : IDisposable
    {
        private SerialPort serialPort { get; set; }

        private static DenkoviConnector instance = null;
        private static readonly object padlock = new object();
        public static string portName = string.Empty;
        //
        private static int buffLen = 0;
        private static byte[] inBuff = new byte[2];
        private static byte[] outBuff = new byte[2];
        private static byte[] tempBuff = new byte[10];





        public DenkoviConnector()
        {
            try
            {
                serialPort = new SerialPort();
                serialPort.PortName = portName;
                serialPort.BaudRate = 9600;
                serialPort.Open();
            }
            catch (Exception ex)
            {
                instance = null;
                serialPort.Close();
                serialPort.Dispose();
            }

            //
            //serialPort.DiscardInBuffer();
            //serialPort.Write("ask//");
            //while (serialPort.BytesToRead < 2) ;
            //buffLen = serialPort.BytesToRead;
            //serialPort.Read(inBuff, 0, 2);
            //UInt16 relaysStatus = (UInt16)((inBuff[0] << 8) + inBuff[1]);

            //getRelays();
            //string deviceDescription = "PCI-1762";
            //instantDoCtrl = new InstantDoCtrl();
            //instantDoCtrl.SelectedDevice = new DeviceInformation(deviceDescription);
        }

        public static DenkoviConnector Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DenkoviConnector();
                    }
                    return instance;
                }
            }
        }


        public void setResetRelays(string command)
        {
            if (serialPort.IsOpen)
            {
                try
                {
                    serialPort.DiscardInBuffer();
                    serialPort.Write(command);
                    while (serialPort.BytesToRead < command.Length) ;
                    buffLen = serialPort.BytesToRead;
                    serialPort.Read(tempBuff, 0, buffLen);
                }
                catch (Exception ex)
                {
                    instance = null;
                    serialPort.Close();
                    serialPort.Dispose();
                    //MessageBox.Show(ee.Message);
                }
            }
            else
            {

            }
            //statusLabel.Text = "Open Port First";
        }

        public void Dispose()
        {
            serialPort.Close();
        }

        //public void Write(string port, string data)
        //{
        //    instantDoCtrl.Write(int.Parse(port), byte.Parse(data));
        //}

        //public void Dispose()
        //{
        //    instantDoCtrl.Dispose();
        //}

        //private void BufferedAiCtrl_Stopped(object sender, BfdAiEventArgs e)
        //{

        //}
    }
}
