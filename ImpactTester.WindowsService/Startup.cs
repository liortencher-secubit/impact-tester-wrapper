﻿using ImpactTester.BL;
using ImpactTester.BL.Validators;
using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;
using Unity;
using Unity.Lifetime;

namespace ImpactTester.WindowsService
{
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            //Thread.Sleep(20000);
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            //Unity
            var container = new UnityContainer();
            container.RegisterType<IImpactTesterRepository, ImpactTesterRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IImpactTesterConnectorRepository, ImpactTesterConnectorRepository>(new HierarchicalLifetimeManager());
            //ContainerControlledLifetimeManager
            config.DependencyResolver = new UnityResolver(container);

            var assembly = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = assembly.Substring(0, assembly.LastIndexOf("\\")) + "\\ImpactTester.API.dll";
            config.Services.Replace(typeof(IAssembliesResolver), new SelfHostAssemblyResolver(path));


            // Web API configuration and services
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            EnableCors(config);
            //
            appBuilder.UseWebApi(config);
        }

        public static void EnableCors(HttpConfiguration config)
        {
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
