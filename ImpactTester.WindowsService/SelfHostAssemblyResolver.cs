﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dispatcher;

namespace ImpactTester.WindowsService
{
    class SelfHostAssemblyResolver : IAssembliesResolver
    {
        string path = string.Empty;
        public SelfHostAssemblyResolver(string path)
        {
            this.path = path;
        }
        public ICollection<Assembly> GetAssemblies()
        {
            List<System.Reflection.Assembly> assemblies = new List<Assembly>();
            assemblies.Add(System.Reflection.Assembly.LoadFrom(path));
            return assemblies;
        }
    }
}
