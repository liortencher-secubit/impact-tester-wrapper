﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ImpactTester.WindowsService
{
    public partial class ImpactTesterWindowsService : ServiceBase
    {
        public ImpactTesterWindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            string baseAddress = $"http://localhost:{ConfigurationManager.AppSettings["ServerPort"].ToString()}/";

            // Start OWIN host 
            WebApp.Start<Startup>(url: baseAddress);
        }

        protected override void OnStop()
        {
        }
    }
}
