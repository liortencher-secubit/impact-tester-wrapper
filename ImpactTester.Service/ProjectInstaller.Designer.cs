﻿namespace ImpactTester.Service
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImpactProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ImpactInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ImpactProcessInstaller
            // 
            this.ImpactProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ImpactProcessInstaller.Password = null;
            this.ImpactProcessInstaller.Username = null;
            // 
            // ImpactInstaller
            // 
            this.ImpactInstaller.ServiceName = "Impact Tester Service";
            this.ImpactInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ImpactInstaller_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ImpactProcessInstaller,
            this.ImpactInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ImpactProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ImpactInstaller;
    }
}