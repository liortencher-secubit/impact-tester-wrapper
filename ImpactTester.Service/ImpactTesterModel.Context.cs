﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ImpactTester.Service
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ImpactTesterEntities : DbContext
    {
        public ImpactTesterEntities()
            : base("name=ImpactTesterEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<dt_Antenna> dt_Antenna { get; set; }
        public virtual DbSet<dt_Counters> dt_Counters { get; set; }
        public virtual DbSet<dt_CounterSteps> dt_CounterSteps { get; set; }
        public virtual DbSet<dt_ImpactTest> dt_ImpactTest { get; set; }
        public virtual DbSet<dt_Step> dt_Step { get; set; }
        public virtual DbSet<dt_StepCommands> dt_StepCommands { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
    }
}
